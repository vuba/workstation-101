@echo off
title alias setter
alias dmu=docker-machine start
alias dmd=docker-machine stop
alias dcu=docker-compose up -d
alias dcd=docker-compose down
alias dcs=docker-compose stop
alias dcl=docker-compose logs -f
alias dps=docker ps -a
alias mku=minikube start
alias mkd=minukube stop
alias d=docker
alias k=kubectl
alias kgs=kubectl get svc
alias kgd=kubectl get deployment
