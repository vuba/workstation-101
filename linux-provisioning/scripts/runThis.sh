#!/bin/sh
INSTALL_PACKAGES="firstTimeSetup rc fonts shell terminal"
SCRIPT_DIRECTORY=$pwd
START_TIME=$(date +%s)
for install_package in $INSTALL_PACKAGES
do
  echo "---- PROVISIONING $install_package ----"
  cd $install_package
  ./*.sh
  cd $SCRIPT_DIRECTORY
done
END_TIME=$(date +%s)
EXECUTION_TIME=$((START_TIME-END_TIME))
echo "---- EXECUTION TIME $EXECUTION_TIME seconds ----"