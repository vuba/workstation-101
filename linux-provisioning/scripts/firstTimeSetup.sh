#!/bin/sh
cd firstTimeSetup
sudo ./1_updateSystem.sh
sudo ./2_installEssentials.sh
sudo ./3_installDocker.sh
sudo ./4_installYarn.sh