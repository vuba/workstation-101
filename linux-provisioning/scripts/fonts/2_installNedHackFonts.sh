#!/bin/sh
cd /usr/local/share/fonts
mkdir nerdhack
#Download latest version from internet
wget https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/Hack/Regular/complete/Hack%20Regular%20Nerd%20Font%20Complete.ttf
cd ..
#refresh system fonts
fc-cache