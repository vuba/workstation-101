#!/bin/sh
OUTPUT_FILE=/Users/vuba/.zshrc
#cd navigate to home folder
echo "#-------CUSTOM ALIASES---------">> $OUTPUT_FILE
echo "alias dcu=\"docker-compose up -d \" ">> $OUTPUT_FILE
echo "alias dcd=\"docker-compose down \" ">> $OUTPUT_FILE
echo "alias dcl=\"docker-compose logs -f \" ">> $OUTPUT_FILE
echo "alias dcs=\"docker-compose stop \" ">> $OUTPUT_FILE
echo "alias dp=\"docker ps -a \" ">> $OUTPUT_FILE
echo "alias dsl=\"docker service ls \" ">> $OUTPUT_FILE
echo "alias d=\"docker\" ">> $OUTPUT_FILE
echo "alias projects=\"cd ~/devTools/projects \" ">> $OUTPUT_FILE
echo "alias ds=\"docker stop \$(docker ps -a -q) \" ">> $OUTPUT_FILE
echo "alias mvnsb=\"mvn clean spring-boot:run\" " >> $OUTPUT_FILE
echo "alias kg=\"kubectl get\"" >> $OUTPUT_FILE
echo "alias k=\"kubectl\"" >> $OUTPUT_FILE
echo "alias gcm=\"git checkout master\"" >> $OUTPUT_FILE
echo "alias gp=\"git pull\"" >> $OUTPUT_FILE
echo "alias gs=\"git status\"" >> $OUTPUT_FILE
