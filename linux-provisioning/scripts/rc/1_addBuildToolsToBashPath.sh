#!/bin/sh
currentUserName="$(whoami)"
OUTPUT_FILE=$1
cd #navigate to home folder
echo "#-------CUSTOM PATH ENVIRONEMNTS---------">> $OUTPUT_FILE
echo "export PATH=/home/$currentUserName/devTools/buildTools/apache-maven/bin:\$PATH" >> $OUTPUT_FILE
echo "export PATH=/home/$currentUserName/devTools/buildTools/gradle/bin:\$PATH" >> $OUTPUT_FILE
echo "export PATH=/home/$currentUserName/devTools/buildTools/node/bin:\$PATH" >> $OUTPUT_FILE
echo "export PATH=/home/$currentUserName/.config/yarn/global/node_modules/.bin:\$PATH" >> $OUTPUT_FILE
echo "export PATH=/home/$currentUserName/.yarn/bin:\$PATH" >> $OUTPUT_FILE
echo "export PATH=/home/$currentUserName/devTools/buildTools/go/bin:\$PATH" >> $OUTPUT_FILE
echo "export PATH=/home/$currentUserName/devTools/buildTools/kotlinc/bin:\$PATH" >> $OUTPUT_FILE