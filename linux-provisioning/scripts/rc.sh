#!/bin/sh
cd rc
OUTPUT_FILE="~/.bashrc_alises"
echo $SHELL
case "$SHELL" in 
  *zsh*)
   OUTPUT_FILE="~/.zshrc_aliases"
    ;;
esac
./addAliassesToPath.sh $OUTPUT_FILE
./addBuildToolsToPath.sh $OUTPUT_FILE
