#!/bin/bash

sudo apt-get install -y python-software-properties
sudo apt-get install -y software-properties-common
#repo for java
sudo add-apt-repository -y ppa:webupd8team/java
#repo for midnight commander
sudo add-apt-repository -y ppa:eugenesan/ppa
sudo apt-get update
# confirm java instalation popup
echo debconf shared/accepted-oracle-license-v1-1 select true | sudo debconf-set-selections
echo debconf shared/accepted-oracle-license-v1-1 seen true | sudo debconf-set-selections
# installs all the dependencies
sudo apt-get install -y git curl make gcc g++ unzip rar unrar tmux \
    vim mc flashplugin-nonfree openssh-server htop oracle-java8-installer \
    ranger zsh spectacles glances
# setUpJava
#update-alternatives --config java
#update-alternatives --config javac
