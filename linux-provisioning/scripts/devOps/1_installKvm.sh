#!/bin/sh
sudo apt-get update
sudo apt install -y -o 'apt::install-recommends=true' \
  qemu-kvm libvirt0 libvirt-bin virt-manager libguestfs-tools
sudo adduser $USER kvm