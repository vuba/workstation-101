#!/bin/sh
echo "$(whoami) ALL=(ALL) NOPASSWD:ALL" >> no_passwd_sudo
sudo chown root:root no_passwd_sudo
sudo chmod 0440 no_passwd_sudo
sudo mv no_passwd_sudo /etc/sudoers.d/