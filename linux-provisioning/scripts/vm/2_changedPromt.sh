#!/bin/sh
echo export PS1="\[\e[33m\]\u\[\e[m\]\[\e[33m\]@\[\e[m\]\[\e[36m\]\h\[\e[m\]: \[\e[33m\]\A\[\e[m\] -- \w :\[\e[35m\]\`parse_git_branch\`\[\e[m\]\[\e[33m\]\\$\[\e[m\] " >> ~/.bashrc