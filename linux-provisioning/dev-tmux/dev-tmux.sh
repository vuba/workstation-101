#!/bin/sh
tmux new-session -d 'htop'
tmux split-window -v -p 85
tmux new-window 'workspace'
tmux -2 attach-session -d
#to resize pane, select it, use macro ( ctrl + b), then hold ctrl and press arrow key in desired direction
#to scroll in panes, press macro (ctrl + b), then press [, then you can scroll with arrows. To exit mode press q.
