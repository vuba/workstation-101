@echo off

@setlocal
SET MACHINE_NAME=%1

%0\..\create_docker_machine.cmd %MACHINE_NAME% & %0\..\docker_machine_env.cmd %MACHINE_NAME% & docker swarm init --advertise-addr 192.168.99.100

@endlocal