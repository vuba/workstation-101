#!/bin/sh
docker ps \
| grep NameOfImage \
| awk '{print $1}' \
| xargs -n 1 -r docker inspect \
-f '{{.ID}} {{.State.Running}} {{.State.StartedAt}}' \
| awk '$2 == "true" && $3 <= "'$(date -d '10 hour ago' -Inc --utc \
| sed 's/+0000/Z/')'" { print $1 }' \
| xargs -f docker kill
