@echo off
@setlocal
SET MACHINE=%1

if ""=="%MACHINE_NAME%" SET MACHINE_NAME=default

docker-machine create --engine-insecure-registry localhost:5000 ^
  --driver "virtualbox" ^
  --virtualbox-memory "2048" ^
  --virtualbox-cpu-count "2" ^
  --virtualbox-disk-size 20000 ^
  %MACHINE_NAME%

@endlocal