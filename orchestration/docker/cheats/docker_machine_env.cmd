@echo off

docker-machine start %1
FOR /f "tokens=*" %%i IN ('docker-machine env %1') DO %%i
set COMPOSE_CONVERT_WINDOWS_PATHS=1