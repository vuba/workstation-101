the keys are used for local playing with VMs. They are configured for use with username 'vuba'

To make a ssh tunnel:
ssh -f identitiFile -L 2000:localhost:5432 -N

Generate ssh-key:
ssh-keygen -t rsa -b 4096 -C "your_email@example.com"