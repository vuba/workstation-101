#!/bin/sh
#create directory
sudo mkdir /home/vuba/.ssh
#copy files
sudo mv /tmp/my_rsa.pub /home/vuba/.ssh/my_rsa.pub
sudo mv /tmp/known_hosts /home/vuba/.ssh/known_hosts
#set permission rights
#sudo chmod 644 /home/vuba/.ssh/my_rsa.pub
#set access right
sudo chown -R vuba /home/vuba/.ssh
sudo chgrp -R vuba /home/vuba/.ssh