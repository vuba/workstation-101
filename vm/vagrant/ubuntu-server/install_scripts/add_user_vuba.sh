#!/bin/sh
echo '[SCRIPT] -> Adding user vuba'
#Crypt3 encoded password with value 'admin'
PASSWORD='$6$ac/3w3oEMl05zKX6$qH/fuDdfzAku3ND5Jr/LvhYh8e9Foe2k.UzoFfMFrHGb.ogcFqFsZZSqVk/01z5QGHbVixsGke/31MiipoFi30'
useradd -m -p $PASSWORD -s /bin/bash vuba
usermod -aG sudo vuba