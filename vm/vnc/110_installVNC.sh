#!/bin/sh
sudo apt-get update
#Install xfce
sudo apt-get install -f xfce4 xfce4-goodies
#Install VNC server
sudo apt-get install -f tightvncserver
cp passwd ~/.vnc/passwd
#For mate window manager
#cp xstartup.mate ~/.vnc/xstartup
#For xfce window manager
xstartup.xfce ~/.vnc/xstartup
sudo cp vncserver.service /etc/systemd/system/vncserver@.service
sudo system daemon-reload
sudo systemctl enable vncserver@.service