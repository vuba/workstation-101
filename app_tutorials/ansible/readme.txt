- when running playbooks, you can specify different inventory files by providing:
  '-i' parameter with the path to the new file.
