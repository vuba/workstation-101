The 'addr' fieldn in the config file is specified as ":5000" to listen to all interfaces.
Setting it to localhost:5000 would only listen to inside the container.