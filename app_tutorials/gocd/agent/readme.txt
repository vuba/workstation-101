When constructing the agent image,
add these lines

COPY agent-bootstrapper-logback-include.xml /opt/go-agent/config/agent-bootstrapper-logback-include.xml
COPY agent-launcher-logback-include.xml /opt/go-agent/config/agent-launcher-logback-include.xml
COPY agent-logback-include.xml /opt/go-agent/config/agent-logback-include.xml


The files can be found at
https://github.com/gocd/docker-gocd-agent

Also, gocd images should have the .ssh credentials set for desired git repo (with chmod 600 for files in .ssh dir)

and include docker inside