Example command:
sudo systemctl {status|start|stop|enable|disable} serviceName

Units should be stored in /etc/systemd/system/serviceName.service example of Unit:

[Unit]
Description=
After=network.target

[Service]
Type=simple
User=root
ExecStart=commandToStartService
ExecStop=commandToStopCommandGracefully
Restart=on-abort

[Install]
WantedBy=multi-user.target