Units should be stored in /etc/systemd/system/serviceName.mount example of Unit:
The name of the file shoud be where-it-will-be-mounted (ex: home-vuk-mounted.mount)

[Unit]
Description=
After=network.target

[Mount]
What = whatToMount
Where = whereToMount
Type = typeOfFileSystem
Options=user=username,password=passwrod,uid=uidNumber

[Install]
WantedBy=multi-user.target