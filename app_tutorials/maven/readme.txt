When creating a maven docker image, 
base the image from a image that supports jdk, 
download the maven bin to /opt/maven

also set the following parameters:

ENV MAVEN_HOME /opt/maven
ENV PATH ${PATH}:${MAVEN_HOME}/bin